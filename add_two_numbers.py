class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        carry = 0
        new_list = None
        new_list_tail = None
        curr1 = l1
        curr2 = l2
        while curr1 or curr2:
            if curr1 and curr2:
                valsum = curr1.val + curr2.val + carry
                curr1 = curr1.next
                curr2 = curr2.next
            elif curr1:
                valsum = curr1.val + carry
                curr1 = curr1.next
            else: # curr2
                valsum = curr2 + carry
                curr2 = curr2.next
            d, carry = valsum % 10, valsum // 10
            new_node = ListNode(d)
            if not new_list:
                new_list = new_node
                new_list_tail = new_node
            else:
                new_list_tail.next = new_node
                new_list_tail=new_node
        return new_list


def construct_link_list(ilist):
    new_list = None
    for i in ilist:
        new_node = ListNode(i)
        new_node.next = new_list
        new_list = new_node
    return new_list


def print_list(llist, name=''):
    current = llist
    l = []
    while current:
        l.append(str(current.val))
        current = current.next
    print(name, ' -> '.join(l))


sln = Solution()
# Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
# Output: 7 -> 0 -> 8
in1 = construct_link_list([2, 4, 3])
in2 = construct_link_list([5, 6, 4])
print_list(in1, 'list1: ')
print_list(in2, 'list2: ')
out = sln.addTwoNumbers(in1, in2)
print_list(out, 'sum: ')
