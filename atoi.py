class Solution(object):
    def myAtoi(self, str):
        """
        :type str: str
        :rtype: int
        """
        ord0 = ord('0')
        ord9 = ord('9')
        ordneg = ord('-')
        ordpos = ord('+')
        val = 0
        isneg = False
        first = True
        for c in str.lstrip():
            ordval = ord(c)
            if ordval < ord0 or ordval > ord9:
                if ordval == ordneg:
                    if first:
                        isneg = True
                        first = False
                        continue
                    else:
                        break
                elif ordval == ordpos:
                    if first:
                        first = False
                        continue
                    else:
                        break
                else:
                    break
            ival = ordval - ord0
            val *= 10
            val += ival
            first = False
        if isneg:
            factor = -1
        else:
            factor = 1
        result = val * factor
        if result < -2147483648:
            return -2147483648
        if result > 2147483647:
            return 2147483647
        return result

sln = Solution()
print(sln.myAtoi("    1232467    "))
print(sln.myAtoi("    1232467A"))
print(sln.myAtoi("  -123"))
print(sln.myAtoi("+2147483647"))
print(sln.myAtoi("-2147483648"))
#these should fail
print(sln.myAtoi("   ABC"))
print(sln.myAtoi("  --123"))
print(sln.myAtoi("2147483648"))
print(sln.myAtoi("-2147483649"))
print(sln.myAtoi("+-5"))
print(sln.myAtoi("-+5"))
