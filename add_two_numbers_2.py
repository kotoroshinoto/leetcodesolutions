class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        from collections import deque
        stk1 = deque()
        stk2 = deque()
        def load_stk(stk, l):
            current = l
            while current:
                stk.append(current)
                current = current.next
        load_stk(stk1, l1)
        load_stk(stk2, l2)
        carry = 0
        new_list = None
        while len(stk1) or len(stk2):
            d1, d2 = stk1.pop(), stk2.pop()
            valsum = d1.val + d2.val + carry
            d, carry = valsum % 10, valsum // 10
            new_node = ListNode(d)
            new_node.next = new_list
            new_list = new_node
        return new_list


def construct_link_list(ilist):
    new_list = None
    for i in ilist:
        new_node = ListNode(i)
        new_node.next = new_list
        new_list = new_node
    return new_list


def print_list(llist, name=''):
    current = llist
    l = []
    while current:
        l.append(str(current.val))
        current = current.next
    print(name, ' -> '.join(l))


sln = Solution()
# Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
# Output: 7 -> 0 -> 8
in1 = construct_link_list([2, 4, 3])
in2 = construct_link_list([5, 6, 4])
print_list(in1, 'list1: ')
print_list(in2, 'list2: ')
out = sln.addTwoNumbers(in1, in2)
print_list(out, 'sum: ')
